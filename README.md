# Home studio - Software for Linux OS

## Free hand drawing

```
yum -y install xournal
```


## Screen recorder:

The best in my opinion in terms of speed, quality and low resource usage is: [Simple Screen Recorder](https://www.maartenbaert.be/simplescreenrecorder/)

Don't fall in the trap of fancy screen recorders.

```
[root@kworkhorse ~]# yum info simplescreenrecorder
Last metadata expiration check: 1:34:01 ago on Sat 06 Mar 2021 06:01:56 PM CET.
Installed Packages
Name         : simplescreenrecorder
Version      : 0.4.3
Release      : 1.fc33
Architecture : x86_64
Size         : 3.7 M
Source       : simplescreenrecorder-0.4.3-1.fc33.src.rpm
Repository   : @System
From repo    : rpmfusion-free-updates
Summary      : Simple Screen Recorder is a screen recorder for Linux
URL          : https://www.maartenbaert.be/simplescreenrecorder/
License      : GPLv3
Description  : It is a screen recorder for Linux.
             : Despite the name, this program is actually quite complex.
             : It's 'simple' in the sense that it's easier to use than ffmpeg/avconv or VLC
```

```
[root@kworkhorse ~]# yum -y install simplescreenrecorder
```

### Use the following settings for best quality and performance:

* Frame rate: `15` is good enough for good quality screen casts.
* Record Audio: PulseAudio
* Video container (aka. video file format): MP4
* Video Codec: H.264
* Preset: superfast
* Audio: MP3
* (audio) Bitrate (in kbit/s): 128
* Enable recording hotkey (good idea): Super + R

**Note:** **Super** is the key mistakenly printed with the windows logo on the keyboard - most of the time)

![images/screen-recorder-1.png](images/screen-recorder-1.png)

![images/screen-recorder-2.png](images/screen-recorder-2.png)

![images/screen-recorder-3.png](images/screen-recorder-3.png)

![images/screen-recorder-4.png](images/screen-recorder-4.png)


## Border-less camera viewer/feed application - CamDesk:

Download the tarball from [https://sourceforge.net/projects/camdesk/files/latest/download](https://sourceforge.net/projects/camdesk/files/latest/download), then perform the following steps:

```
cd ~/Downloads

tar xzf camdesk-1.1-lin.tar.gz

sudo mkdir /opt/camdesk

sudo mv CamDesk/* /opt/camdesk/

sudo chmod +x /opt/camdesk/electron
```

Next, create a `Desktop` file in certain location. This location depends on whether you want this to be available for *all users* on this system, or just a singe user.
* For all users: `/usr/share/applications/`
* For single/current user: `~/.local/share/applications/`

```
[kamran@kworkhorse Downloads]$ vi ~/.local/share/applications/camdesk.desktop

[Desktop Entry]
Type=Application
Encoding=UTF-8
Name=CamDesk
Comment=Borderless webcam viewer for screen-casting
Icon=/opt/camdesk/resources/default_app/logo.svg
Exec=/opt/camdesk/electron
Terminal=false
Categories=webcam;viewer;Application
```


## Video Editing:

I absolutely recommend FlowBlade, because of it's simplicity, intuitive interface, and low resource utilization.

```
[root@kworkhorse ~]# yum -y install flowblade
```

![images/flow-blade.png](images/flow-blade.png)


# Hardware:

## Sound absorption panel:
Home-made sound absorption panels: [https://youtu.be/pABvTWSxOes](DIY Sound Absorption panels: https://youtu.be/pABvTWSxOes)


## Digital pen for free hand writing / drawing:
**Optional.** Bamboo digital pen - or an equivalent by any other maker/manufacturer.

![https://images-na.ssl-images-amazon.com/images/I/513e4a5FGAL._AC_SL1000_.jpg](https://images-na.ssl-images-amazon.com/images/I/513e4a5FGAL._AC_SL1000_.jpg)

## Microphone:

**Optional, but recommended**. Any good quality simple microphone.

## Web camera:
**Optional**. Any good quality external web cam preferably "HD quality" will do.
